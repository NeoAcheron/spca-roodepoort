import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent }          from './home/component';
import { ReportCrueltyComponent } from './report-cruelty/component';
import { AboutUsComponent }       from './about-us/component';
import { ContactUsComponent }     from './contact-us/component';
import { DonateComponent }     from './donate/component';

const routes: Routes = [
  { path: '', component: HomeComponent },  
  { path: 'report-cruelty', component: ReportCrueltyComponent },  
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'donate', component: DonateComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports:[
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
