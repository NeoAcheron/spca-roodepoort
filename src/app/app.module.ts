import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Globals } from './globals';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AboutUsComponent } from './about-us/component';
import { HomeComponent } from './home/component';
import { ReportCrueltyComponent } from './report-cruelty/component';
import { ContactUsComponent } from './contact-us/component';
import { DonateComponent } from './donate/component';


@NgModule({
  declarations: [
    AppComponent,
    AboutUsComponent,
    HomeComponent,
    ReportCrueltyComponent,
    ContactUsComponent,
    DonateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [Globals],
  bootstrap: [AppComponent]
})
export class AppModule { }
