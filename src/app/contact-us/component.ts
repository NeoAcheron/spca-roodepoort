import { Component, OnInit } from '@angular/core';

import { Globals } from '../globals';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-contact-us',
  templateUrl: './template.html',
  styleUrls: ['./style.scss']
})
export class ContactUsComponent implements OnInit {
  public phoneGeneral: string = "";
  public phoneEmergency: string = "";

  constructor(public globals: Globals, public appComponent: AppComponent){    
    this.phoneGeneral = globals.phoneGeneral;
    this.phoneEmergency = globals.phoneEmergency;
    appComponent.footer = false;
  }

  ngOnInit() {    
  }
  
}
