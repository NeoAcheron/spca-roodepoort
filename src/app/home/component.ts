import { Component, OnInit } from '@angular/core';

import { Globals } from '../globals';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-home',
  templateUrl: './template.html',
  styleUrls: ['./style.scss']
})
export class HomeComponent implements OnInit {

  constructor(public globals: Globals, public appComponent: AppComponent){    
    appComponent.footer = true;
  }

  ngOnInit() {
  }

}
