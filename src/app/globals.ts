import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
    public phoneGeneral: string = '011 672 0448';
    public phoneEmergency: string = '076 070 1400';
}